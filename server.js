// server.js

// modules
var express = require('express');
var session = require('express-session');
var app = express();
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var jade = require('jade');

var server = require('./config/server');

// Express middleware to populate 'req.body' so we can access POST variables
app.use(bodyParser.json());

// parse application/vnd.api+json as json
app.use(bodyParser.json({type: 'application/vnd.api+json'}));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: true}));

// Enable simulation of HTTP methods other than GET and POST ie simulate DELETE, PUT
app.use(methodOverride('X-HTTP-Method-Override'));

// Set views base directory, and set the view engine to be jade.
app.set('views', __dirname + '/app/views');
app.set('view engine', 'jade');

// set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/public'));

// required for passport
app.use(session({secret: 'thefours.org mongoose lime'})); // session secret

// routes ==================================================
require('./app/routes/routes')(app); // configure our routes

// start app
app.listen(server.port, server.ipaddr);

// log server start
console.log('Server listening on port: ' + server.port + ', ip : ' + server.ipaddr);



