(function() {
	'use strict';

	angular.module('app')
		.controller('IndexController', IndexController);

	IndexController.$inject = ['$scope', 'stateService', '$mdDialog', 'sportService', '$route', '$location', '$anchorScroll'];

	function IndexController($scope, stateService, $mdDialog, sportService, $route, $location, $anchorScroll) {
		var vm = this;
		vm.menuOpen = false;

		vm.state = stateService;

		vm.icon = "menu";
		vm.closeIsHome = false;

		vm.sport = $route.current.params.sport;
		vm.competition = $route.current.params.competition;
		vm.event = $route.current.params.event;

		vm.hasEvent = !!vm.event;

		if (vm.hasEvent ) {
			vm.icon = "home";
			vm.closeIsHome = true;
		}

		vm.toggleMenu = toggleMenu;
		vm.selectDay = selectDay;

		activate(vm.sport, vm.competition, vm.event);

		function activate(sport, competition, event) {

			stateService.set(sport, competition, event);

			if (event) {
				vm.event = sportService.getEvent(sport, competition, event);
			}
			else if (competition) {
				vm.dates = sportService.getEventsByDate(sport, competition)
			}
			else if (sport) {
				vm.dates = sportService.getEventsByDate(sport);
			}
			else {
				// home...
			}
		}

		function refresh() {
			vm.sport = vm.state.selected.sport;
			vm.competition = vm.state.selected.competition;
			vm.event = vm.state.selected.event;

			vm.events = null;
			vm.competitions = null;

			vm.state = stateService;

			activate(vm.sport, vm.competition, vm.event);
		}

		function toggleMenu(event) {

			if (stateService.hasEvent()) {
				stateService.backUp();
				vm.hasEvent = false;
				vm.event = null;

				vm.icon = 'menu';

				if (vm.closeIsHome) {
						stateService.set();
						vm.closeIsHome = false;

						return
				}

				stateService.set(vm.state.selected.sport, vm.state.selected.competition);

				return;
			}

			if (!vm.menuOpen) {

				$mdDialog.show({
					templateUrl: 'js/dialogs/menu.tmpl.html',
					parent: angular.element(document.body),
					targetEvent: event,
					clickOutsideToClose: true,
					controller: DialogController
				}).then(function(answer) {
					// positive action resolution / cb
				}, function() {
					// dialog cancel resolution / cb
					// update with new params on close, ew
					refresh();
					vm.menuOpen = false;
					vm.icon = "menu";
				});

				vm.icon = "arrow_upward";
				vm.menuOpen = true;
			}
			else {
				$mdDialog.hide();
				vm.menuOpen = false;
				vm.icon = "menu";
			}
		}

		function selectDay(day) {
			$location.hash(day);
			$anchorScroll();
		}
	}
}());


function DialogController($scope, $mdDialog) {
	$scope.cancel = function() {
		$mdDialog.cancel();
	};
}
