(function () {
    'use strict';

    angular.module('app')
        .config(['$routeProvider', '$locationProvider',
            function ($routeProvider, $locationProvider) {

            $routeProvider
                .when('/', {
                    templateUrl: 'views/index.html',
                    controller: 'IndexController',
                    controllerAs: 'vm'
                })
                .when('/:sport', {
                    templateUrl: 'views/index.html',
                    controller: 'IndexController',
                    controllerAs: 'vm'
                })
                .when('/:sport/:competition', {
                    templateUrl: 'views/index.html',
                    controller: 'IndexController',
                    controllerAs: 'vm'
                })
                .when('/:sport/:competition/:event', {
                    templateUrl: 'views/index.html',
                    controller: 'IndexController',
                    controllerAs: 'vm'
                });

            $locationProvider.html5Mode(true);
        }]);
}());
