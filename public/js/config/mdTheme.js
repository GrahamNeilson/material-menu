(function () {
    'use strict';

    angular.module('app')
        .config(mdTheme);

    mdTheme.$inject = ['$mdThemingProvider'];

    function mdTheme($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('indigo', {
            'default': '400', // by default use shade 400 from the pink palette for primary intentions
            'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
            'hue-2': '800', // use shade 600 for the <code>md-hue-2</code> class
            'hue-3': '600' // use shade A100 for the <code>md-hue-3</code> class
            })
            .accentPalette('deep-orange')

    }
}());