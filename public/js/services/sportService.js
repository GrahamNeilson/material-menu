(function () {
    'use strict';

    angular.module('app')
        .factory('sportService', sportService);

    function sportService() {

        var rootSports = ['football', 'tennis', 'basketball'];
        var competitions = {
            football: ['premier-league', 'championship', 'league-1', 'league-2', 'Serie-A', 'Serie-B','Serie-C', 'Serie-D'],
            tennis: ['Wimbledon', 'US-Open', 'Australian-Open', 'French-Open'],
            basketball: ['NBA', 'English-League', 'Croatia']
        };

        var sports = Immutable.fromJS(bootstrap({}, rootSports, competitions));

        return {
            getEventsByDate: getEventsByDate,
            getAll: getAll,
            getCompetitions: getCompetitions,
            getEvent: getEvent
        };

        function bootstrap(sports, rootSports, competitions) {
            rootSports.forEach(function (sport) {
                sports[sport] = {};
                sports[sport].name = sport;
                sports[sport].events = {};

                createSportsEvents(sports[sport].events, competitions[sport], sport);
            });

            return sports;
        }

        function createSportsEvents(rootNode, competitions, sport) {

            for (var k = 0; k < competitions.length; k++) {
                const competition = competitions[k];
                const participant = 'team ';

                const required = 10;
                for (var i = 0; i < required; i++) {
                    const date = new Date();
                    // 3 per day
                    date.setDate(date.getDate() + Math.floor(i / 3));

                    rootNode[i + 1 + (k * 10)] = {
                        name: participant + '-1-' + participant + '2',
                        home: participant + '1',
                        away: participant + '2',
                        competition: competition,
                        sport: sport,
                        id: i,
                        date: date
                    };
                }
            }
        }


        /*
         Service implementation
         */
        function getAll() {
            return sports;
        }

        function getCompetitions(sport) {

            return sports
                    .getIn([sport, 'events'])
                    .sortBy(function (event) {
                        return event.get('competition');
                    })
                    .groupBy(function (event) {
                        return event.get('competition');
                    })
                    .keySeq()
                    .toArray();
        }

        function getEvent(sport, competition, event) {
          var ev = sports.get([sport, 'events', event])
          //debugger;

          return ev;

        }

        function getEventsByDate(sport, competition) {

            var events;

            if (!competition) {
                events = sports.getIn([sport, 'events']);
            }
            else {
                events = sports
                            .getIn([sport, 'events'])
                            .filter(function(event) {
                                return event.get('competition') === competition;
                            });
            }

            events = events
                .sortBy(function (event) {
                    return event.get('date');
                })
                .groupBy(function (event) {
                    return event.get('date');
                });

            const keys = events.keySeq();

            keys.forEach(function (key) {
                const grouped = events
                    .get(key)
                    .sortBy(function (event) {
                        return event.get('competition');
                    })
                    .groupBy(function (event) {
                        return event.get('competition');
                    });

                events = events.set(key, grouped);
            });

            console.log(events.toJS());
            return events.toJS();

        }
    }

}());
