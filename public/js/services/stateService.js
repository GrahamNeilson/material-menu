(function () {
    'use strict';

    angular.module('app')
        .factory('stateService', stateService);

    stateService.$inject = ['$location', 'sportService'];

    function stateService($location) {

        var selected = {
            sport: null,
            event: null,
            competition: null,
            competitions: ['premier league', 'championship'] // default switches, say
        };

        var previousPath = null;
        var getCurrentPath = function() {
            return '/football';
        };

        return {
            selected: selected,
            previousPath: previousPath,
            sport: selected.sport,
            event: selected.event,
            competitions: selected.competitions,
            competition: selected.competition,

            hasEvent: function () {
                return selected.event;
            },

            getEvent: function () {
                return selected.event;
            },

            reset: function () {
                selected.competition = null;
                selected.event = null;
                selected.sport = null;

                $location.path('/', false);
            },

            backUp: function() {
                selected.event = null;

                var path =  previousPath;

                previousPath = null;

                $location.path(path, false);
            },

            set: function (sport, competition, event) {

                this.reset();

                selected.sport = sport;

                if (event) {
                    selected.event = event;

                    var path = ('/' + sport + '/' + competition + '/' + event);
                    path = path.replace(/ /g, '-');
                    $location.path(path, false);
                }
                else if (competition) {
                    selected.competition = competition;

                    var path = ('/' + sport + '/' + competition);
                    path = path.replace(/ /g, '-');
                    previousPath = getCurrentPath();
                    $location.path(path, false);
                }
                else if (sport) {
                    var path = ('/' + sport);
                    path = path.replace(/ /g, '-');
                    previousPath = getCurrentPath();
                    $location.path(path, false);
                }
            }
        }
    }

}());
