(function () {
    'use strict';

    angular.module('app')
        .directive("eventList", function () {
            return {
                restrict: 'E',
                replace: false,
                templateUrl: '/js/directives/eventList/eventList.html',
                scope: {
                    events: "="
                },
                controller: function ($scope) {
                    setTimeout(function() {
                        console.log($scope.events)
                    }, 0);
                }
            };
        })
}());
