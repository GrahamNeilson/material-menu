(function () {
    'use strict';

    angular.module('app')
        .directive("sportMenu", function () {
            return {
                restrict: 'E',
                replace: false,
                templateUrl: '/js/directives/sportMenu/sportMenu.html',
                scope: {
                    sport: "=sport",
                    cancel: "&"
                },
                controller: ['$scope', 'sportService', 'stateService', function($scope, sportService, stateService) {

                    $scope.competitions = sportService.getCompetitions($scope.sport);

                    $scope.selectCompetition = function(competition) {
                        stateService.set($scope.sport, competition);
                        $scope.cancel();
                    }

                    $scope.selectSport = function(sport) {
                        stateService.set(sport);
                        $scope.cancel();
                    }
                }]
            };
        })
}());