(function () {
    'use strict';

    angular.module('app')
        .directive("competitionList", function () {
            return {
                restrict: 'E',
                replace: false,
                templateUrl: '/js/directives/competitionList/competitionList.html',
                scope: {
                    competitions: "="
                },
                controller: function ($scope) {

                }
            }
        })
}());
