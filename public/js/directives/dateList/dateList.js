(function () {
    'use strict';

    angular.module('app')
        .directive("dateList", function () {
            return {
                restrict: 'E',
                replace: false,
                templateUrl: '/js/directives/dateList/dateList.html',
                scope: {
                    dates: "="
                },
                controller: function ($scope) {

                }
            };
        })
}());
