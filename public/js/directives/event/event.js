(function () {
    'use strict';

    angular.module('app')
        .directive("event", function () {
            return {
                restrict: 'E',
                replace: false,
                templateUrl: '/js/directives/event/event.html',
                scope: {
                    event: "=event"
                },
                controller: function ($scope, stateService) {
                    $scope.selectEvent = function() {
                        stateService.set($scope.event.sport, $scope.event.competition, $scope.event.name);
                    }
                }
            };
        })
}());
