(function () {
    'use strict';

    angular.module('app')
        .directive("list", function () {
            return {
                restrict: 'E',
                replace: false,
                templateUrl: '/js/directives/list/list.html',
                scope: {
                    dates: "="
                },
                controller: function ($scope, stateService) {

                    $scope.selectEvent = function(event) {
                        stateService.set(event.sport, event.competition, event.name);
                    }
                }
            };
        })
}());
