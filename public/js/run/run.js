(function () {

    'use strict';

    // allow an extra variable on the location.path method : false;
    // do not update the route.
    // http://joelsaupe.com/programming/angularjs-change-path-without-reloading/
    angular.module('app')
        .run(routeChangeNoReload);

    routeChangeNoReload.$inject = ['$route', '$rootScope', '$location'];

    function routeChangeNoReload($route, $rootScope, $location) {
        var original = $location.path;
        $location.path = function (path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function () {
                    $route.current = lastRoute;
                    un();
                });
            }

            return original.apply($location, [path]);
        };
    }
}());
